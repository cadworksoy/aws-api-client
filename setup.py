import sys
from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need fine tuning.
build_exe_options = {"packages": ['idna'], "excludes": []}

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None

# if sys.platform == "win32":
#    base = "Win32GUI"

setup(  name = "aws_api_client",
        version = "0.1",
        description = "Command-line client for AutomateWorks Server",
        options = {"build_exe": build_exe_options},
        executables = [Executable("aws-api-client.py", base=base)])