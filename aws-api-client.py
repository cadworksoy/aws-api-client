#!/usr/bin/env python

import argparse
import requests
import getpass
import logging
import urlparse
import urllib
import sys
import json
import time
from uuid import UUID
import os.path
import shutil
from decimal import Decimal
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler


FORMAT = '%(asctime)s %(levelname)s: %(message)s'

logger = logging.getLogger(__name__)


def get_auth_token(args):
    """
    Demonstrates how to get auth token from api.

    See: https://aws.cadworkssoftware.com/api/docs/#token-auth-create

    :param args: Parsed command line arguments from argparse.
    :return: Returns auth token if login succeeded.
    """

    logger.info("Logging into {0} in as {1}".format(args.baseurl, args.user))

    url = urlparse.urljoin(args.baseurl, "/api/token-auth/")
    data = {"username": args.user, "password": args.pwd}

    response = requests.post(url=url, data=data)
    if response.status_code == requests.codes.ok:
        logger.debug("Login succeeded.")
        result = response.json()
        logger.debug("Access token: {0}...".format(result['token'][:10]))
        return result
    else:
        raise Exception("Login failed, r: {0}".format(response.status_code))



def list_configurators(args):
    """
    Demonstrates how to list existing configurators. Outputs list of configurators returned by server.

    See: https://aws.cadworkssoftware.com/api/docs/#v1-configurators-list

    :param args: Parsed command line arguments from argparse.
    """
    logger.info("Fetching list of configurators.")

    url = urlparse.urljoin(args.baseurl, "/api/v1/configurators/")
    token = get_auth_token(args)['token']
    headers = {"authorization": "JWT {0}".format(token)}
    params = {"limit": args.limit, "offset": args.offset}

    response = requests.get(url=url, params=params, headers=headers)

    if response.status_code == requests.codes.ok:
        logger.debug("Fetching configurators succeeded.")
        data = response.json()
        if args.dump == False:
            logger.info("Server retuned {0} of {1} configurators:".format(len(data['results']), data['count']))
            for c in data['results']:
                logger.info(u"Id: {uuid}, Name: {name}".format(**c))
        return data
    else:
        raise Exception("Fetching configurators failed, server returned: {0}.".format(response.status_code))

def list_workers(args):
    """
    Demonstrates how to list existing workers. Outputs list of workers returned by server.

    See: https://aws.cadworkssoftware.com/api/docs/#v1-workers-list

    :param args: Parsed command line arguments from argparse.
    """
    logger.info("Fetching list of workers.")

    url = urlparse.urljoin(args.baseurl, "/api/v1/workers/")
    token = get_auth_token(args)['token']
    headers = {"authorization": "JWT {0}".format(token)}
    params = {"limit": args.limit, "offset": args.offset}

    response = requests.get(url=url, params=params, headers=headers)

    if response.status_code == requests.codes.ok:
        logger.info("Fetching workers succeeded.")
        data = response.json()
        if args.dump == False:
            logger.info("Server retuned {0} of {1} workers:".format(len(data['results']), data['count']))
            for w in data['results']:
                logger.info(u"Id: {uuid}, Name: {name}, Is online: {is_online}".format(**w))
        return data
    else:
        raise Exception("Fetching workers failed, result: {0}.".format(response.status_code))


def list_jobs(args):
    """
    Demonstrates how to list existing jobs. Outputs list of jobs returned by server.

    See: https://aws.cadworkssoftware.com/api/docs/#v1-jobs-list

    :param args: Parsed command line arguments from argparse.
    """
    logger.info("Fetching list of workers.")

    url = urlparse.urljoin(args.baseurl, "/api/v1/jobs/")
    token = get_auth_token(args)['token']
    headers = {"authorization": "JWT {0}".format(token)}
    params = {"limit": args.limit, "offset": args.offset}

    response = requests.get(url=url, params=params, headers=headers)

    if response.status_code == requests.codes.ok:
        logger.info("Fetching jobs succeeded.")
        data = response.json()
        if args.dump == False:
            logger.info("Server retuned {0} of {1} jobs:".format(len(data['results']), data['count']))
            for j in data['results']:
                logger.info(u"Id: {uuid}, Status: {status[message]}".format(**j))
        return data
    else:
        raise Exception("Fetching workers failed, result: {0}.".format(response.status_code))

def get_configurator(args):
    """
    Demonstrates how to get details of single configurator. Outputs configurator details.

    See: https://aws.cadworkssoftware.com/api/docs/#v1-configurator-read

    :param args: Parsed command line arguments from argparse.
    :return: Returns the configurator data from server.
    """
    logger.info("Fetching single configurator.")

    url = urlparse.urljoin(args.baseurl, "/api/v1/configurator/{0}/".format(args.id))
    token = get_auth_token(args)['token']
    headers = {"authorization": "JWT {0}".format(token)}

    response = requests.get(url=url, headers=headers)


    if response.status_code == requests.codes.ok:
        logger.info("Fetching configurator succeeded:")
        data = response.json()
        if args.dump == False:
            logger.info(u"Id: {uuid}, Name: {name}, Path: {path}".format(**data))
            if 'workers' in data:
                logger.info("Workers:")
                for w in data['workers']:
                     logger.info(u"\tId: {uuid}, Name: {name}".format(**w))
            if 'input_fields' in data:
                logger.info("Input fields:")
                for i in data['input_fields']:
                     logger.info(u"\tName: {name}, Type: {type[name]}".format(**i))
        return data
    else:
        raise Exception("Fetching configurator failed, result: {0}.".format(response.status_code))

def get_worker(args):
    """
    Demonstrates how to get details of single worker. Outputs worker details.

    See: https://aws.cadworkssoftware.com/api/docs//#v1-worker-read

    :param args: Parsed command line arguments from argparse.
    """
    logger.info("Fetching single worker.")

    url = urlparse.urljoin(args.baseurl, "/api/v1/worker/{0}/".format(args.id))
    token = get_auth_token(args)['token']
    headers = {"authorization": "JWT {0}".format(token)}

    response = requests.get(url=url, headers=headers)


    if response.status_code == requests.codes.ok:
        logger.info("Fetching worker succeeded:")
        data = response.json()
        if args.dump == False:
            logger.info("Id: {uuid}, Name: {name}, Online: {is_online}".format(**data))
            if 'workstation' in data:
                workstation = data['workstation']
                logger.info("Workstation:")
                logger.info("\tName: {name}, Organization: {organization}".format(**workstation))
            if 'configurators' in data:
                logger.info("Configurators:")
                for c in data['configurators']:
                     logger.info("\tId: {uuid}, Name: {name}".format(**c))
        return data
    else:
        raise Exception("Fetching worker failed, result: {0}.".format(response.status_code))

def get_job(args):
    """
    Demonstrates how to get details of single job. Outputs worker details.

    See: https://aws.cadworkssoftware.com/api/docs//#v1-job-read

    :param args: Parsed command line arguments from argparse.
    """
    logger.info("Fetching single worker.")

    url = urlparse.urljoin(args.baseurl, "/api/v1/job/{0}/".format(args.id))
    token = get_auth_token(args)['token']
    headers = {"authorization": "JWT {0}".format(token)}

    response = requests.get(url=url, headers=headers)

    if response.status_code == requests.codes.ok:
        logger.info("Fetching job succeeded:")
        data = response.json()
        if args.dump == False:
            logger.info("Id: {uuid}, Status: {status[message]}".format(**data))
            if 'worker' in data:
                worker = data['worker']
                if worker is not None:
                    logger.info(u"Worker: {name}".format(**worker))
            if 'configurators' in data:
                logger.info("Configurators:")
                for c in data['configurators']:
                     logger.info("\tId: {uuid}, Name: {name}".format(**c))
        return data
    else:
        raise Exception("Fetching worker failed, result: {0}.".format(response.status_code))


def get_file(args):
    """
    Demonstrates how to get details of a file. Outputs file details.

    See: https://aws.cadworkssoftware.com/api/docs//#v1-file-read

    :param args: Parsed command line arguments from argparse.
    """
    logger.info("Fetching single file.")

    url = urlparse.urljoin(args.baseurl, "/api/v1/file/{0}/".format(args.id))
    token = get_auth_token(args)['token']
    headers = {"authorization": "JWT {0}".format(token)}

    response = requests.get(url=url, headers=headers)

    if response.status_code == requests.codes.ok:
        logger.info("Fetching file succeeded:")
        data = response.json()
        if args.dump == False:
            logger.info("Id: {uuid}".format(**data))
        return data
    else:
        raise Exception("Fetching worker failed, result: {0}.".format(response.status_code))

def download_file(args):
    """
    Demonstrates how to download a file.

    See: https://aws.cadworkssoftware.com/api/docs//#v1-file-download-read

    :param args: Parsed command line arguments from argparse.
    """
    logger.info("Downloading single file.")

    # Get file metadata first
    file = get_file(args)
    # Check if file exists:
    if os.path.isfile(file['name']):
        raise Exception("File to be downloaded already exists.")

    url = urlparse.urljoin(args.baseurl, "/api/v1/file/{0}/download/".format(args.id))
    token = get_auth_token(args)['token']
    headers = {"authorization": "JWT {0}".format(token)}

    with requests.get(url=url, headers=headers) as response:
        if response.status_code == requests.codes.ok:
            with open(file['name'], 'wb') as f:
                for chunk in response.iter_content(chunk_size=1024):
                    if chunk:
                        f.write(chunk)
            logger.info("Downloading file succeeded.")
        else:
            raise Exception("Downloading file failed, result: {0}.".format(response.status_code))

def cast_to(raw_value, field_type):
    """
    Cast value to type defined by input field.
    :param raw_value: Raw value from user
    :param field_type: Input field type from aws api
    """
    types = {'String': str, 'Decimal': Decimal, 'Boolean': bool, 'Integer': int}
    if field_type['name'] not in types:
        raise Exception("Invalid field type: {0}".format(field_type))
    return types[field_type['name']](raw_value)


def read_job_variables(args):
    """
    Read job variables as specified in command line arguments.
    :param args: Parsed command line arguments.
    :return: Returns variable valus as dictionary.
    """

    # Get configurator from server
    configurator = get_configurator(args)
    if configurator == None:
        logger.error("Failed to get configurator from server.")
        return None

    # Convert list of input field to dictionary
    
    input_field_values = {}
    if args.mode == 'prompt':
        # Prompt for each input field value
        for field in configurator['input_fields']:
            raw_value = raw_input("Enter {type[name]} value for {name}: ".format(**field))
            input_field_values[field['name']] = cast_to(raw_value, field['type'])
    elif args.mode == 'stdin':
        # Read raw values from stdin
        logger.info("Reading values from stdin.")
        input_fields = {field['name']: field for field in configurator['input_fields']}
        for line in sys.stdin:
            name, raw_value = line.split("=")
            name = name.strip()
            if name in input_fields:
                # Cast value to correct type
                field = input_fields[name]
                input_field_values[name] = cast_to(raw_value, field['type'])
    else:
        # Upload variable values later by using upload-variables method
        pass

    logger.debug("Input field values:")
    logger.debug(input_field_values)
    return input_field_values

def create_job(args):
    """
    Demonstrates how to create a job for a configurator.

    See: https://aws.cadworkssoftware.com/api/docs//#v1-configurator-job-create

    :param args: Parsed command line arguments from argparse.
    """
    logger.info("Creating a job for a configurator..")

    inputs = read_job_variables(args)
    if inputs == None:
        raise Exception("Reading variable values failed.")

    url = urlparse.urljoin(args.baseurl, "/api/v1/configurator/{0}/job/".format(args.id))
    token = get_auth_token(args)['token']
    headers = {"authorization": "JWT {0}".format(token)}
    data = {'inputs': json.dumps(inputs, cls=DecimalEncoder)}
    logger.info(data)
    response = requests.post(url=url, headers=headers, data=data)

    if response.status_code == requests.codes.created:
        data = response.json()
        logger.info("Creating new job request succeeded:")
        if args.dump == False:
            logger.info("Job request id: {uuid}".format(**data))
            logger.info("Job request status: {job_request_status[message]}".format(**data))
            if data['job'] is not None:
                job = data['job']
                logger.info("Job Id: {uuid}".format(**job))
                logger.info("Job Status: {status[message]}".format(**job))
        if args.mode == 'upload':
            if args.filename is not None:
                args.id = data['uuid']
                upload_variables(args)

        return data
    else:
        raise Exception("Creating job failed, result: {0}, {1}.".format(response.status_code, response.json()))


def upload_variables(args):
    """
    Demonstrates how to upload variables as a file to a job request..

    See: https://aws.cadworkssoftware.com/api/docs/#v1-job-request-upload-create

    :param args: Parsed command line arguments from argparse.
    """

    if os.path.isfile(args.filename) == False:
        raise Exception("Failed to upload variable file: file not found.")

    if args.id:
        logger.info("Uploading variables to a job.")
        url = urlparse.urljoin(args.baseurl, "/api/v1/job-request/{0}/variables/upload/".format(args.id))
        data = None
    else:
        logger.info("Creating new job.")
        data = {'configurator_variable_name': args.configurator_variable_name}
        url = urlparse.urljoin(args.baseurl, "/api/v1/job-request/upload/")

    token = get_auth_token(args)['token']
    headers = {"authorization": "JWT {0}".format(token)}
    with open(args.filename, 'rb') as file:
        files = {'file': file }
        response = requests.post(url=url, headers=headers,
            data=data, files=files)
    if response.status_code == requests.codes.created:
        data = response.json()
        logger.info("Uploading variables succeeded:")
        if args.dump == False:
            logger.info("File id: {uuid}".format(**data))

        return data
    else:
        raise Exception("Uploading variables failed, result: {0}, {1}.".format(response.status_code, response.json()))


def move_file_to(source_filename, target_directory_name):
    """
    Helper function to move file and generate new filename if file already exists.
    """
    path, extension = os.path.splitext(source_filename)
    path, filename = os.path.split(path)
    new_path = os.path.join(target_directory_name,
            "{}{}".format(filename, extension))

    index = 0
    while os.path.isfile(new_path):
        index += 1
        new_path = os.path.join(target_directory_name,
            "{}-{}{}".format(filename, index, extension))

    logger.info("Moving {0} to {1}".format(source_filename, new_path))
    os.rename(source_filename, new_path)


class JobFileEventHandler(PatternMatchingEventHandler):
    """
    Event handler which creates a new job for each new file to aws.
    """

    def __init__(self, cmd_args, *args, **kwargs):
        super(JobFileEventHandler, self).__init__(*args, **kwargs)
        self._cmd_args = cmd_args

    @property
    def arguments(self):
        return self._cmd_args

    def on_created(self, event):
        logger.info("***File created: %s" % event.src_path)

        args = self.arguments
        args.id = None
        args.filename = event.src_path
        try:
            result = upload_variables(args)

            if self.arguments.success_path:
                move_file_to(event.src_path, self.arguments.success_path)

            return
        except requests.ConnectionError as e:
            logger.error("Failed to upload file:")
            logger.error(e.message)
        except:
            logger.error(sys.exc_info()[1])
        
        if self.arguments.error_path:
            move_file_to(event.src_path, self.arguments.error_path)

class DecimalEncoder(json.JSONEncoder):
    """
    Helper class to encode decimal values as float in json.
    """

    def default(self, o):
        if isinstance(o, Decimal):
            return float(o)
        return super(DecimalEncoder, self).default(o)

def run_feeder(args):
    event_handler = JobFileEventHandler(args, patterns=['*.txt'])
    observer = Observer()
    observer.schedule(event_handler, args.input_path, recursive=False)
    observer.start()
    logger.info("Observing incoming task files in {0}.".format(args.input_path))
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
    logger.info("Finished observing task files.")


def main():
    """
    Parse command line arguments
    """
    # Common arguments
    parser = argparse.ArgumentParser(description="Demonstrate AWS api.")
    parser.add_argument('--user', required=False, help="User name to use when logging in.")
    parser.add_argument('--pwd', required=False, help="Password to use when logging in.")
    parser.add_argument('--baseurl', required=False, default="https://aws.cadworkssoftware.com/")
    parser.add_argument('--loglevel', required=False, type=int, default=logging.INFO, help="Log level.")
    parser.add_argument('--dump', required=False, type=bool, default=False, help="If set to True, dump result as json to output.")


    subparsers = parser.add_subparsers(help="Action to perform.")

    # Arguments for get-auth-token command
    auth_parser = subparsers.add_parser("get-auth-token", help="Get auth token")
    auth_parser.set_defaults(func=get_auth_token)

    # Arguments for list-configurators command
    subparser = subparsers.add_parser("list-configurators", help="List configurators")
    subparser.add_argument('--limit', required=False, type=int, help="Limit count of results.")
    subparser.add_argument('--offset', required=False, type=int, help="Offset count of results.")
    subparser.set_defaults(func=list_configurators)

    # Arguments for list-workers command
    subparser = subparsers.add_parser("list-workers", help="List workers")
    subparser.add_argument('--limit', required=False, type=int, help="Limit count of results.")
    subparser.add_argument('--offset', required=False, type=int, help="Offset count of results.")
    subparser.set_defaults(func=list_workers)

    # Arguments for list-workers command
    subparser = subparsers.add_parser("list-jobs", help="List workers")
    subparser.add_argument('--limit', required=False, type=int, help="Limit count of results.")
    subparser.add_argument('--offset', required=False, type=int, help="Offset count of results.")
    subparser.set_defaults(func=list_jobs)

    # Arguments for get-configurator command
    subparser = subparsers.add_parser("get-configurator", help="Get configurator")
    subparser.add_argument('--id', required=True, type=UUID, help="Id of the configurator to get.")
    subparser.set_defaults(func=get_configurator)

    # Arguments for get-worker command
    subparser = subparsers.add_parser("get-worker", help="Get worker")
    subparser.add_argument('--id', required=True, type=UUID, help="Id of the worker to get.")
    subparser.set_defaults(func=get_worker)

    # Arguments for create-job command
    subparser = subparsers.add_parser("create-job", help="Create job for a configurator.")
    subparser.add_argument('--id', required=True, type=UUID, help="Id of the configurator to which the job should be added.")
    subparser.add_argument('--mode', required=True, choices=['prompt', 'stdin', 'upload'], type=str, default='prompt', help="Defines how to read variable values.")
    subparser.add_argument('--filename', required=False, type=str, help="Filename of variable file to be uploaded.")
    subparser.set_defaults(func=create_job)

    # Arguments for get-worker command
    subparser = subparsers.add_parser("get-job", help="Get job")
    subparser.add_argument('--id', required=True, type=UUID, help="Id of the job to get.")
    subparser.set_defaults(func=get_job)

    # Arguments for upload-variables command
    subparser = subparsers.add_parser("upload-variables", help="Upload variables for a job request.")
    subparser.add_argument('--id', required=False, type=UUID, help="Id of the job request to which the variables should be uploaded.")
    subparser.add_argument('--filename', required=True, type=str, help="Filename of variable file to be uploaded.")
    subparser.add_argument('--configurator_variable_name', required=False, type=str, help="Name of variable containing configurator name.")
    subparser.set_defaults(func=upload_variables)

    # Arguments for get-file command
    subparser = subparsers.add_parser("get-file", help="Get file")
    subparser.add_argument('--id', required=True, type=UUID, help="Id of the file to get.")
    subparser.set_defaults(func=get_file)

    # Arguments for download-file command
    subparser = subparsers.add_parser("download-file", help="Download file")
    subparser.add_argument('--id', required=True, type=UUID, help="Id of the file to download.")
    subparser.set_defaults(func=download_file)

    # Arguments for download-file command
    subparser = subparsers.add_parser("run-feeder", help="Run Feeder")
    subparser.add_argument('--configurator_variable_name', required=True, type=str, help="Name of variable containing configurator name.")
    subparser.add_argument('--input_path', required=True, type=str, help="Path to observe for incoming files.")
    subparser.add_argument('--success_path', required=False, type=str, help="Path into which succeeded input files are moved.")
    subparser.add_argument('--error_path', required=False, type=str, help="Path into which succeeded input files are moved.")
    subparser.set_defaults(func=run_feeder)

    args = parser.parse_args()
    logging.basicConfig(format=FORMAT, level=args.loglevel)
    if args.user is None:
        args.user = raw_input("Enter username:")
    if args.pwd is None:
        args.pwd = getpass.getpass("Enter password:")

    # Perform requested action
    try:
        result = args.func(args)
        if args.dump == True and result is not None:
            logger.info(json.dumps(result, indent=4, sort_keys=True))
    except:
        e = sys.exc_info()
        logger.error("Operation failed: {0}".format(e[1]))

if __name__ == "__main__":
    main()
