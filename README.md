# AutomateWorks API Demo
This is a simple script demonstrating how to use AutomateWorks Server REST API.

## Installation instructions:

### Pre-requirements:
- Git
- Python 2.7
- Pip

#### Recommended:
- [Virtualenv](https://virtualenv.pypa.io/en/stable/)
- [Virtualenvwrapper](https://virtualenv.pypa.io/en/stable/)

### Installation instructions:
To setup aws api demo, clone this repository, create a virtual environment
eg. by using virtualenvwrapper and install requirements:

```
git clone git@bitbucket.org:cadworksoy/aws-api-client.git
cd aws-api-client
mkvirtualenv aws-api-client
pip install -r requirements.txt
```

## How to use:

Run aws-api-client.py in shell to see list of command line arguments.
Use *-h* command line argument to show detailed help.

```

./aws-api-client.py -h


```
